package main

import (
	"src/api"
	"src/db"
)

func main() {
	// Inisialisasi DB
	db.Init(false)
	// Inisialisasi Server
	e := api.Init()

	// Server Listener
	e.Logger.Fatal((e.Start(":1323")))
}
