package routes

import (
	"github.com/labstack/echo/v4"
	"github.com/rs/zerolog/log"
)

func Init(e *echo.Echo) {
	log.Info().Msg("menginisialisasikan routes")
	routes1(e)
	routes2(e)
	log.Info().Msg("routes terinisialisasi")
}
