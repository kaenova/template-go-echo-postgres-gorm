package db

import (
	"fmt"
	"src/config"
	"src/utils/errlogger"

	"github.com/rs/zerolog/log"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

var db *gorm.DB = nil
var err error

func Init(dataInitialization bool) {

	log.Info().Msg("menginisialisasikan database")

	config := config.GetConfig()

	dsn := fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=disable",
		config.Database.Host,
		config.Database.Port,
		config.Database.Username,
		config.Database.Password,
		config.Database.Name)
	db, err = gorm.Open(postgres.Open(dsn), &gorm.Config{SkipDefaultTransaction: true})
	errlogger.ErrFatalPanic(err)

	if dataInitialization {
		initData(db)
	}

	log.Info().Msg("database terinisialisasi")
}

func GetDB() *gorm.DB {
	if db == nil {
		errlogger.FatalPanicMessage("db belum terinisilisasi")
	}
	return db
}
