package db

import "gorm.io/gorm"

func Migration(db *gorm.DB) {
	/*
		Please fill the params in AutoMigrate with your entity
		so you will see
		db.AutoMigrate(&Entity1{}, &Entity2{}, &Entity3{}, ...)
	*/
	db.AutoMigrate()
}
